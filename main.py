#!/usr/bin/python
#coding=utf-8
from  jinja2 import  Environment,FileSystemLoader
import os,argparse,sys
base_dir=os.path.dirname(os.path.abspath(__file__))


def build_configuration(namespace,project,config_dir,nodeport,force,targetport,area):
    app_name = project
    app_namespace =namespace
    config_dir = config_dir
    force = force
    targetport = targetport
    nodeport = nodeport
    area = area
    env = Environment(loader=FileSystemLoader("/etc/k8s/"))
    app_template = env.get_template("app-template.yml")
    app_template_service = env.get_template("app-template-service.yml")
    os.system("mkdir -p %s" % config_dir)
    name_app_name_yml = app_name + ".yml"
    name_app_name_service = app_name + "-service.yml"

    file_dir_name_app_name_yml = config_dir + os.sep + name_app_name_yml
    file_dir_name_app_name_service = config_dir + os.sep + name_app_name_service

    if not force:
        if os.path.isfile(file_dir_name_app_name_yml) and os.path.isfile(file_dir_name_app_name_service):
            print  "k8s部署配置文件存在，不处理"
            sys.exit(0)
        if nodeport:
            content_app_template = app_template.render(app_name=app_name,app_namespace=app_namespace,nodeport=nodeport,targetport=targetport,area=area)
            with open(file_dir_name_app_name_yml,"w") as f:
                f.write(content_app_template)

            content_app_template_service = app_template_service.render(app_name=app_name,app_namespace=app_namespace,nodeport=nodeport,targetport=targetport)
            with open(file_dir_name_app_name_service,"w") as f:
                f.write(content_app_template_service)
        else:
            content_app_template = app_template.render(app_name=app_name,app_namespace=app_namespace,nodeport=nodeport,targetport=targetport,area=area)
            with open(file_dir_name_app_name_yml,"w") as f:
                f.write(content_app_template)
    else:
        if nodeport:
            content_app_template = app_template.render(app_name=app_name, app_namespace=app_namespace,
                                                       nodeport=nodeport,targetport=targetport,area=area)
            with open(file_dir_name_app_name_yml, "w") as f:
                f.write(content_app_template)

            content_app_template_service = app_template_service.render(app_name=app_name, app_namespace=app_namespace,
                                                                       nodeport=nodeport,targetport=targetport)
            with open(file_dir_name_app_name_service, "w") as f:
                f.write(content_app_template_service)
        else:
            content_app_template = app_template.render(app_name=app_name, app_namespace=app_namespace,
                                                       nodeport=nodeport,targetport=targetport,area=area)
            with open(file_dir_name_app_name_yml, "w") as f:
                f.write(content_app_template)



def parser_init():
    parser = argparse.ArgumentParser(description="k8s部署配置生成")
    parser.add_argument("--namespace", help="k8s命名空间")
    parser.add_argument("--project", help="项目名称")
    parser.add_argument("--config_dir", help="配置生成路径")
    parser.add_argument("--nodeport",default=None,help="节点端口,默认为空")
    parser.add_argument("--force",default=None,help="强制覆盖已存在配置，默认关闭")
    parser.add_argument("--targetport",default=None,help="容器端口,默认为空")
    parser.add_argument("--area",default=None,help="区域配置service或...")
    args = parser.parse_args()
    return args

def run():
    args =parser_init()
    if not (args.namespace and args.project and args.config_dir):
        print "错误 必填参数不符"
        sys.exit(0)
    build_configuration(namespace=args.namespace, project=args.project, config_dir=args.config_dir,
                        nodeport=args.nodeport, force=args.force,targetport=args.targetport,area=args.area)

if __name__ == '__main__':
    run()
